---


title: 使用Vercel搭建一个静态网站

sticky: 1

tags:

- 网站
- Free

description:
- 干货

categories:
- Vercel

abbrlink: 98e384e2

date: 2020-12-11 21:53:28

cover: "[https://cdn.jsdelivr.net/gh/glahajeekn/imcdn@main/images/20201212180846.gif](https://cdn.jsdelivr.net/gh/glahajeekn/imcdn@main/images/20201212180846.gif)"

---

提供免费搭建网站的服务有很多，像热铁盒主机、Coding Pages、GitHub Pages······Vercel 也是其中之一，今天教大家使用 Vercel 搭建一个静态网站。

# 登录账号

[前往 Vercel 官网](https://www.vercel.com)

![](https://cdn.jsdelivr.net/gh/glahajeekn/imcdn@main/images/20201212171545.png#alt=image-20201212171544981)

点击 Login 按钮

点击后会跳转至这个页面

![](https://cdn.jsdelivr.net/gh/glahajeekn/imcdn@main/images/20201212171715.png#alt=image-20201212171715452)

# 授权服务

选择一个你有的账号

按钮 1.连接到 GitHub

按钮 2.连接到 GitLab

按钮 3.连接到 Bitbucket

如果你都没有就可以点击下面的 Don't have an account? Sign Up(没有一个账号?注册)

这里用 GitHub 来演示

登录完成后会跳转到仪表盘

![](https://cdn.jsdelivr.net/gh/glahajeekn/imcdn@main/images/20201212172119.png#alt=image-20201212172119038)

这时候我们先回到 GitHub，我们先创建一个仓库放入测试 html

# 导入项目

接着我们复制刚创建的仓库的 git 地址

![](https://cdn.jsdelivr.net/gh/glahajeekn/imcdn@main/images/20201212172440.png#alt=image-20201212172440181)

![](https://cdn.jsdelivr.net/gh/glahajeekn/imcdn@main/images/20201212172456.png#alt=image-20201212172456843)

![](https://cdn.jsdelivr.net/gh/glahajeekn/imcdn@main/images/20201212172552.png#alt=image-20201212172552337)进入这个页面的时候注意，由于我们的仓库里放了一个 html，所以这里不要做任何设置直接点击蓝色的 Deploy 按钮

![](https://cdn.jsdelivr.net/gh/glahajeekn/imcdn@main/images/20201212172705.png#alt=image-20201212172704922)

出现这个页面就代表你的网页成功部署了

我们点击 Visit 按钮去测试一下

![](https://cdn.jsdelivr.net/gh/glahajeekn/imcdn@main/images/20201212172947.png#alt=image-20201212172947775)

成功!

这样就可以搭建一个简单的静态网站了。

对于 Hexo 你可以直接放源码上去，然后配置构建命令，不过这个内容不属于本文范围之类，所以就不讲了。

# Thanks For You
