---


title: 如何创建自己的Office E5

sticky: 1

abbrlink: 143f7edb

date: 2020-05-14 17:38:44

tags:

description:

- 白嫖
- Office

cover: "[https://cdn.jsdelivr.net/gh/slblog-github/BlogFlies/Blog/Pic/2356Cover.png](https://cdn.jsdelivr.net/gh/slblog-github/BlogFlies/Blog/Pic/2356Cover.png)"

---

注：本文转载自 MBRjun 的学习记录

[https://blog.mbrjun.cn/archives/10/](https://blog.mbrjun.cn/archives/10/)

教程首先点击这里，然后点击 Join Now

然后登录，似乎个人和工作教育账户都可以，也可以 Github 登录![](https://cdn.slblog.ga/pic/post/2020514/1.png#)

小编（营销号内味）用 Windows Hello 登录给大家演示

![](https://cdn.slblog.ga/pic/post/2020514/2.png#)

登录你的账户

填写信息，国家地区填你所在的（本项需如实填写，有验证，并且决定后续的绑定），公司可以随便填，下面两个必须都勾选才能成功白嫖，然后下一步

![](https://cdn.slblog.ga/pic/post/2020514/4.png#)

下面的信息如图填写，自己填写没白嫖到 Micorosft365 别找我第一组勾选个人

![](https://cdn.slblog.ga/pic/post/2020514/5.png#)

下面一组全部勾选，然后下一步

![](https://cdn.slblog.ga/pic/post/2020514/6.png#)

点击关闭即可

点击设置订阅

![](https://cdn.slblog.ga/pic/post/2020514/7.png#)

设置你的账户和密码 不用多说了吧 国家和地区决定了电话验证码地区

![](https://cdn.slblog.ga/pic/post/2020514/8.png#)

手机上接收下验证码

![](https://cdn.slblog.ga/pic/post/2020514/9.png#)

等待分配资源

![](https://cdn.slblog.ga/pic/post/2020514/10.png#)

分配完成

![](https://cdn.slblog.ga/pic/post/2020514/11.png#)

到这里你已经成功的白嫖到了 Microsoft 365，点击查看资源开始使用

续期方法：

使用 Oneindex 上传文件

前提：OneDrive 必须分配完成
