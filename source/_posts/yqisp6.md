---


title: 如何使用在线工具 - Photopea

sticky: 1

abbrlink: c9f23913

date: 2020-10-02 17:24:42

tags:

- 实用工具

description: 本地PS无响应?试试在线PS工具

categories:
- 实用工具
- 在线工具

cover: "[https://cdn.jsdelivr.net/gh/slblog-github/BlogFlies/Blog/Pic/image-20201002175504855.png](https://cdn.jsdelivr.net/gh/slblog-github/BlogFlies/Blog/Pic/image-20201002175504855.png)"

---

众所周知，Adobe 全家桶的"PS"是处理图片不可缺少的一款工具

可稍不注意就会

**无响应警告**

不过国外有一位大神开发出了在线版本的"PS"，并且将"PS"的大部分功能都给还原了

这是由 Ivan Kuckir 大神开发的[Photopea](https://www.photopea.com/)

我们首先打开这个网站

![](https://cdn.jsdelivr.net/gh/slblog-github/BlogFlies/Blog/Pic/image-20201002173033869.png#alt=image-20201002173033869)

点击新建项目，当然也可以从电脑打开

接下来它会让我们选择项目大小和模板之类的

![](https://cdn.jsdelivr.net/gh/slblog-github/BlogFlies/Blog/Pic/image-20201002173225825.png#alt=image-20201002173225825)

这里可以直接不选择模板直接选择大小，然后填写一下名字就创建

进入项目后可以看到这样的界面

![](https://cdn.jsdelivr.net/gh/slblog-github/BlogFlies/Blog/Pic/image-20201002173336634.png#alt=image-20201002173336634)

接下来我演示怎么做一个简单的 Logo

首先点击左边工具栏的"T"(文字工具)![](https://cdn.jsdelivr.net/gh/slblog-github/BlogFlies/Blog/Pic/image-20201002173419179.png#alt=image-20201002173419179)

接着在上方会出现![](https://cdn.jsdelivr.net/gh/slblog-github/BlogFlies/Blog/Pic/image-20201002173450316.png#alt=image-20201002173450316)

这里我们可以自己调节字体、文字大小、文字颜色等

这里我选的是 Hanalei Fill 字体

大小我调节的是 499px

然后写入自己想要的文字

写入完成后是这个样子的

![](https://cdn.jsdelivr.net/gh/slblog-github/BlogFlies/Blog/Pic/image-20201002174430487.png#alt=image-20201002174430487)

接下来我们贴个图

去[iconfont](https://iconfont.cn)找个好康的图标，下载下来 png

下载下来 PNG 后![](https://cdn.jsdelivr.net/gh/slblog-github/BlogFlies/Blog/Pic/image-20201002174611722.png#alt=)将这个图片拖进 Photopea

然后调好大小就完成了

![](https://cdn.jsdelivr.net/gh/slblog-github/BlogFlies/Blog/Pic/image-20201002174652493.png#alt=image-20201002174652493)

接着点击文件-另存为 PSD 保存这个工程文件，方便下次修改

然后点击![](https://cdn.jsdelivr.net/gh/slblog-github/BlogFlies/Blog/Pic/image-20201002174746827.png#alt=image-20201002174746827)

然后就可以下载下来 Logo 了

顺便这个网站还提供 SVG 下载哦

总结:齐全的功能，多语言支持，佩服作者 👍
