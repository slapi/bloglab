---
title: 友情链接
sticky: 1
date: 2020-05-17 15:43:50
type: "link"
comments: false
aside: false
---
<script>if(typeof(Friend)=='undefined'){
  // 这个地址按照你的地址填写，主要是为了防止pjax不加载重新刷新页面
  location.href='/link'
}
try {
  btf.isJqueryLoad(function () {
    $('.flink').prepend("<div id='friend1'></div>")
  })
} catch (error) {
  window.onload = function () {
    btf.isJqueryLoad(function () {
      $('.flink').prepend("<div id='friend1'></div>")
    })
  }
}
</script><div id="myfriend"></div><script src="https://cdn.jsdelivr.net/npm/jquery@latest/dist/jquery.min.js"></script><script src='https://unpkg.com/butterfly-friend/dist/friend.min.js'></script></script><script> var obj({        el: '#friend1',        owner: "slqwq",        repo: 'Friends',        direction_sort: "asc",        labelDescr: {            Gitee友链: "<span style='color:red;'>这是通过Gitee Issues添加的小伙伴们哦!</span>",            乐特大佬: "<span style='color:red;'>这可是乐特大佬专属的哦!</span>",        }    })</script>




# 申请友链

{% folding cyan open, 申请须知 %}

{% checkbox green checked, 已使用HTTPS %}

{% checkbox green checked, 无广告 %}

{% checkbox green checked, 转载文章需留原文 %}

{% checkbox green checked, 您的网站是学习/博客/日记的 %}

{% checkbox red checked, 如果您长时间没有更新文章/网站无法访问/取消本站链接本站将直接移除您的友链 %}

{% tabs 个人信息 %}
<!-- tab -->
```
name: SL's Blog
link: https://blog.slqwq.cn
avatar: https://cdn.slblog.ga/pic/logo.png
descr: 梦中做梦,做了个白日梦
```
<!-- endtab -->
{%  endtabs %}

申请方法:

前往[Gitee](https://gitee.com/slqwq/Friends)添加Issues，当管理员审核通过后方可显示

但在这之前，请先确保您已添加本站友链！

详细申请方法已记载到仓库的README.md，按照MD指示方可完成

{% endfolding %}



{% folding red cyan open, 过期的友链们 %}

{% timenode 萌城·博客 %}

名称:萌城·博客 地址:https://men******qwq.com/

{% endtimenode %}

过期的友链请尽快找我重新申请

{% endfolding %}